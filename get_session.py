import requests

CS_ADDRESS = "10.250.241.17" # CM7148-2
API_VERSION = "v1.6"

USERNAME = "root"
PASSWORD = "default"
PASSWORD = "snodgrass"

url = "https://%s/api/%s/sessions" % (CS_ADDRESS, API_VERSION)
body = {
    "username": USERNAME,
    "password": PASSWORD
}
response = requests.post(url, json=body, verify=False)
print(response.json()["session"])