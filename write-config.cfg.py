
Threads = 1

print("""
[global]
run_time = 600
rampup = 0
results_ts_interval = 5
progress_bar = on
console_logging = on
xml_report = off
""")

for port_num in range(1, 49):

    block ="""
[user_group-{port_num}]
threads = {threads}
script = PUT_serialPorts_baudrate.py
run_args = {port_num}
""".format(threads=Threads, port_num=port_num)

    print(block)

