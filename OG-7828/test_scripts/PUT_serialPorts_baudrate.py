import json
import random
import time
import requests
import traceback
from requests.exceptions import RequestException
from requests.models import Response

# Silence that annoying log
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# CS_ADDRESS = "192.168.125.103" # CM7196A-2
CS_ADDRESS = "10.250.241.17" # CM7148-2 The local console is accessible from: sshpass -p default ssh root@testimdmz.test.bne.opengear.com -p 3017
API_VERSION = "v1.6"

USERNAME = "root"
PASSWORD = "default"
PASSWORD = "snodgrass"


def get_session():
    """Logs into CS and returns the session token for subsequent API calls

    Returns:
        [type]: [description]
    """
    url = "https://%s/api/%s/sessions" % (CS_ADDRESS, API_VERSION)
    body = {
        "username": USERNAME,
        "password": PASSWORD
    }
    response = requests.post(url, json=body, verify=False)
    if response.status_code != 200:
        raise RequestException("Failed to post to sessions endpoint. status code = %d, response body = %s" % (response.status_code, response.text))

    return response.json()["session"]


def set_to_default(session_token, port):
    """Sets the port to default

    Args:
        port ([type]): [description]
    """
    
    url = "https://%s/api/%s/serialPorts/port%d" % (CS_ADDRESS, API_VERSION, port)
    headers = {
        "Authorization": "Token %s" % session_token
    }
    body = {
        "serialport": {
            "label": "Port %d" % port,
            "id": "port%d" % port,
            "modeSettings": {
                "disabled": {}
            },
            "attachedDeviceType": "none",
            "mode": "disabled",
            "hardwareSettings": {
                "pinout": "X2",
                "protocol": "RS232",
                "uart": {
                    "parity": "none",
                    "baud": "9600",
                    "stopBits": "1",
                    "dataBits": "8",
                    "flowControl": "none",
                    "dtrMode": "alwayson"
                }
            },
            "ctrlCode": {
                "quit": 0,
                "generateBreak": 0,
                "help": 0,
                "power": 0,
                "chooser": 0,
                "portLog": 0
            },
            "hardwareType": "builtInUART",
            "nagios": {
                "enabled": False,
                "serialStatus": False,
                "name": "",
                "portLogging": False
            },
            "logging": {
                "priority": "default",
                "facility": "default",
                "level": "disabled"
            }
        }
    }
    response = requests.put(url, json=body, headers=headers, verify=False)

    # print(response.json())


def get_current_port_settings(session_token, port):
    url = "https://%s/api/%s/serialPorts/port%d" % (CS_ADDRESS, API_VERSION, port)
    headers = {
        "Authorization": "Token %s" % session_token
    }
    response = requests.get(url, headers=headers, verify=False)
    if response.status_code != 200:
        raise RequestException("Failed to post to Get serialPorts endpoint. status code = %d, response body = %s" % (response.status_code, response.text))

    get_response_payload = response.json()

    return get_response_payload


consoleserver_mode_settings = json.loads('{"consoleServer":{"ssh":{"enabled":true,"unauthenticated":false},"telnet":{"enabled":false,"unauthenticated":false},"general":{"replaceBackspace":false,"escapeChar":"~","singleConnection":false,"accumulateMS":0,"powerMenuEnabled":false},"portShare":{"password":"","authentication":false,"enabled":false,"encryption":false},"rfc2217":{"enabled":false},"webShell":{"enabled":false},"tcp":{"enabled":false},"ipAlias":{"wan":[],"oobfo":[],"wlan":[],"lan":[]}}}')


def change_port_config(session_token, port, get_response_payload):
    """Queries the current port baud rate then changes it to something else
    and PUTs it back

    Args:
        session_token ([type]): [description]
        port ([type]): [description]
    """

    current_baud = get_response_payload["serialport"]["hardwareSettings"]["uart"]["baud"]

    baud_options = ["50", "75", "110", "134", "150", "200", "300", "600", "1200", "1800", "2400", "4800", "9600", "19200", "38400", "57600", "115200", "230400"]

    # Set the baud rate to the next one in the last of options
    new_baud = baud_options[(baud_options.index(current_baud) + 1) % len(baud_options)]
    get_response_payload["serialport"]["hardwareSettings"]["uart"]["baud"] = new_baud

    # Toggle the mode
    current_mode = get_response_payload["serialport"]["mode"]
    new_mode = "consoleServer" if current_mode == "disabled" else "disabled"
    get_response_payload["serialport"]["mode"] = new_mode
    if new_mode == "consoleServer":
        get_response_payload["serialport"]["modeSettings"].update(consoleserver_mode_settings)
    else: # disabled - add the disabled block
        get_response_payload["serialport"]["modeSettings"].update({"disabled": {}})

    # Put the request back
    url = "https://%s/api/%s/serialPorts/port%d" % (CS_ADDRESS, API_VERSION, port)
    headers = {
        "Authorization": "Token %s" % session_token
    }
    response = requests.put(url, headers=headers, json=get_response_payload, verify=False)
    if response.status_code != 200:
        raise RequestException("Failed to PUT to serialPorts endpoint. status code = %d, response body = %s" % (response.status_code, response.text))
    put_response_payload = response.json()

    # print(get_response_payload)
    # print(put_response_payload)

    # Assert the values returned in the PUT response body matches that, that was set
    actual_baud_rate = put_response_payload["serialport"]["hardwareSettings"]["uart"]["baud"]
    assert actual_baud_rate == new_baud, \
        "Error: The baud rate returned in PUT response was %s but expected %s" % (actual_baud_rate, new_baud)

    actual_mode = put_response_payload["serialport"]["mode"]
    assert actual_mode == new_mode, \
        "Error: The mode returned in PUT response was %s but expected %s" % (actual_mode, new_mode)

class Transaction(object):
    def __init__(self):
        pass

    def run(self, run_args=None):
        """
        The custom timer keys in here must all be 1 word.

        Args:
            run_args ([type], optional): [description]. Defaults to None.

        Returns:
            [type]: [description]
        """

        try:
            serial_port = int(run_args)
            
            # Authenticate with the REST API
            # start_time = time.time()
            # session_token = get_session()
            # self.custom_timers["1-POST-sessions"] = time.time() - start_time

            session_token = "9457158f8dcb3fe575648690beaac490"

            # Get the current serialport status
            start_time = time.time()
            get_response_body = get_current_port_settings(session_token, serial_port)
            self.custom_timers["2-GET-serialPorts_portId"] = time.time() - start_time

            # Change the serial port config
            start_time = time.time()
            change_port_config(session_token, serial_port, get_response_body)
            self.custom_timers["3-PUT-serialPorts_portId"] = time.time() - start_time

            print("Success")

        except Exception as e:
            print("The exception is: ", e)
            # breakpoint()
            print("fail")
            print(traceback.format_exc())
            raise e


if __name__ == '__main__':
    trans = Transaction()
    trans.run(1)
    
    print("Done")
