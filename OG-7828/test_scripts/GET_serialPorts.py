import json
import random
import time
import requests
import traceback
from requests.exceptions import RequestException
from requests.models import Response

# Silence that annoying log
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# CS_ADDRESS = "192.168.125.103" # CM7196A-2
CS_ADDRESS = "10.250.241.17" # CM7148-2
API_VERSION = "v1.6"

USERNAME = "root"
# PASSWORD = "default"
PASSWORD = "snodgrass"


def get_current_port_settings(session_token, port):
    url = "https://%s/api/%s/serialPorts/port%d" % (CS_ADDRESS, API_VERSION, port)
    headers = {
        "Authorization": "Token %s" % session_token
    }
    response = requests.get(url, headers=headers, verify=False)
    if response.status_code != 200:
        raise RequestException("Failed to Get serialPorts endpoint. status code = %d, response body = %s" % (response.status_code, response.text))

    get_response_payload = response.json()

    return get_response_payload


class Transaction(object):
    def __init__(self):
        pass

    def run(self, run_args=None):
        """
        The custom timer keys in here must all be 1 word.

        Args:
            run_args ([type], optional): [description]. Defaults to None.

        Returns:
            [type]: [description]
        """

        try:
            serial_port = int(run_args)

            session_token = "9ceb8f257f370c6010c438eaa0c3a3e0"

            # Get the current serialport status
            start_time = time.time()
            get_response_body = get_current_port_settings(session_token, serial_port)
            self.custom_timers["2-GET-serialPorts_portId"] = time.time() - start_time

            print("Success")

        except Exception as e:
            print("The exception is: ", e)
            # breakpoint()
            print("fail")
            print(traceback.format_exc())
            raise e


if __name__ == '__main__':
    trans = Transaction()
    trans.run(1)
    
    print("Done")
